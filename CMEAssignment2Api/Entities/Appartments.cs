﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMEAssignment2Api.Entities
{
    public class Appartments
    {
        public int id { get; set; }
        public String title { get; set; }
        public Decimal price { get; set; }
        public String address { get; set; }
        public int nbOfRooms { get; set; }
    }
}
