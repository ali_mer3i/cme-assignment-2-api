﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMEAssignment2Api.Enteties
{
    public class BuyersEntity
    {
        public int id { get; set; }
        public String fullName { get; set; }
        public decimal credit { get; set; }
    }
}
