﻿using AutoMapper;
using CMEAssignment2Api.Models;
using CMEAssignment2Api.Entities;

namespace CMEAssignment2Api.Enteties
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BuyersEntity, BuyerModel>();
            CreateMap<Appartments, AppartmentModel>();

        }
    }
}

