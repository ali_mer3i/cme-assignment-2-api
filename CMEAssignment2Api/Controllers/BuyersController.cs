﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using CMEAssignment2Api.Enteties;
using CMEAssignment2Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace CMEAssignment2Api.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class BuyersController : ControllerBase
    {
        private Db_Context _db;
        private readonly IMapper _mapper;

        public BuyersController(Db_Context db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;

        }

        // GET api/buyers
        [HttpGet]
        public IEnumerable<BuyerModel> Get()
        {
            //var result = _db.Buyers.Join(_db.OwnedProperties,
            //    buyer => buyer.id,
            //    owned => owned.buyerId,
            //    (buyer, owned) => new BuyerModel { id = buyer.id, fullName = buyer.fullName, credit = buyer.credit, nbOdPurchased = 1 });

            var result1 = (from buyer in _db.Buyers
                           join owned in _db.OwnedProperties on buyer.id equals owned.BuyerId into ow
                           select new BuyerModel { Id = buyer.id, FullName = buyer.fullName, Credit = buyer.credit, NbOfPurchased = ow.Count() }
            );
            var model = _mapper.Map<IEnumerable<BuyerModel>>(result1);

            return model;

        }

        [HttpPost]
        public ActionResult<BuyerModel> Post([FromBody] BuyersEntity buyer)
        {
            _db.Buyers.Add(buyer);
            if (!Save())
            {
                return BadRequest();
            }
            return Created("DefaultApi", buyer);
        }



        public bool Save()
        {
            //return (_db.SaveChanges() >= 0);

            try
            {
                return (_db.SaveChanges() >= 0);
            }
            catch (DbUpdateException)
            {
                throw;

            }
        }
    }
}