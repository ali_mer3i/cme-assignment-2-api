﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CMEAssignment2Api.Enteties;
using CMEAssignment2Api.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using CMEAssignment2Api.Entities;

namespace CMEAssignment2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppartmentsController : ControllerBase
    {
        private Db_Context _db;
        private readonly IMapper _mapper;
        private int pricePerRoom = 15000;

        public AppartmentsController(Db_Context db, IMapper mapper) {
            _db = db;
            _mapper = mapper;

        }

        // GET api/apartments
        [HttpGet]
        public IEnumerable<AppartmentModel> Get()
        {
            var model = _mapper.Map<IEnumerable<AppartmentModel>>(_db.Appartments);

            return model;

        }

        // GET api/apartments/bybuyer/{buyerId}
        [HttpGet]
        [Route("bybuyer/{buyerId}")]
        public IEnumerable<AppartmentModel> GetAppartmentsByBuyer(int buyerId)
        {
            var result1 = (from appartment in _db.Appartments
                           join owned in _db.OwnedProperties on appartment.id equals owned.ApartmentId
                           where owned.BuyerId == buyerId
                           select appartment
                       );
            var model = _mapper.Map<IEnumerable<AppartmentModel>>(result1);

            return model;

        }

        // POST api/apartments
        [HttpPost]
        public ActionResult<AppartmentModel> Post([FromBody] Appartments appartment)
        {
            appartment.price = appartment.nbOfRooms * pricePerRoom;
            _db.Appartments.Add(appartment);
            if (!Save()) {
                return BadRequest();
            }
            return Created("DefaultApi", appartment);
        }

        // POST api/apartments/filter
        [HttpPost]
        [Route("filter")]
        public ActionResult<AppartmentModel> PostFilter([FromBody] AppartmentsFilterModel appartmentFilter)
        {
            var query = _db.Appartments.Where(a => a.address.ToLower().Contains(appartmentFilter.Address.ToLower()));
            if (appartmentFilter.FromPrice != null) {
                query = query.Where(a => a.price >= appartmentFilter.FromPrice);
            }
            if (appartmentFilter.ToPrice != null)
            {
                query = query.Where(a => a.price <= appartmentFilter.ToPrice);
            }
            if (appartmentFilter.NbOfRooms != null)
            {
                query = query.Where(a => a.nbOfRooms == appartmentFilter.NbOfRooms);
            }
           var result = query.OrderByDescending(x => x.price).ToList();


            return Ok(result);
        }

        // PUY api/apartments/{id}
        [HttpPut]
        [Route("{id}")]
        public ActionResult PutApparmtent(int id,[FromBody] Appartments appartment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != appartment.id)
            {
                return BadRequest();
            }

            appartment.price = appartment.nbOfRooms * pricePerRoom;
            _db.Entry(appartment).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AppartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // DELETE: api/Countries/5
        [HttpDelete]
        [Route("{id}")]
        public ActionResult DeleteAppartment(int id)
        {
            Appartments appartment = _db.Appartments.Find(id);
            if (appartment == null)
            {
                var error = new ErrorModel
                {
                    Title = "Delete Failed",
                    Message = "Appartment not found"
                };
                return BadRequest(error);
            }

            var owned = _db.OwnedProperties.Where(o => o.ApartmentId == id);
            if (owned.Count() > 0)
            {
                var error = new ErrorModel
                {
                    Title = "Delete Failed",
                    Message = "Appartment cant be deleted. Owned by a buyer"
                };
                return BadRequest(error);
            }

            _db.Appartments.Remove(appartment);
            if (!Save())
            {
                return BadRequest();
            }
            return Ok(appartment);
        }

        //purchaseappartment
        [HttpGet]
        [Route("purchase/{appartmentId}/{buyerId}")]
        public ActionResult PurchaseAppartment(int appartmentId, int buyerId)
        {
            //fetch buyer and if doesnt exist return error
            BuyersEntity buyer = _db.Buyers.Find(buyerId);
            if (buyer == null)
            {
                var error = new ErrorModel
                {
                    Title = "Purchase Failed",
                    Message = "Buyer not found"
                };
                return BadRequest(error);
            }
            //fetch appartment and if doesnt exist return error
            Appartments appartment = _db.Appartments.Find(appartmentId);
            if (appartment == null)
            {
                var error = new ErrorModel
                {
                    Title = "Purchase Failed",
                    Message = "Appartment not found"
                };
                return BadRequest(error);
            }

            int purchaseCount = _db.OwnedProperties.Where(p => p.ApartmentId == appartment.id).Count();
            if (purchaseCount > 0) {

                var error = new ErrorModel
                {
                    Title = "Purchase Failed",
                    Message = "Appartment already purchased"
                };
                return BadRequest(error);
            }

            //if buyer have enough credit purchase else return error
            if (buyer.credit >= appartment.price)
            {

                buyer.credit -= appartment.price;
                _db.Entry(buyer).State = EntityState.Modified;

                OwnedProperties owned = new OwnedProperties { ApartmentId = appartment.id, BuyerId = buyer.id };
                _db.OwnedProperties.Add(owned);
                if (Save()) {
                    return Ok();
                }
                return BadRequest();
            }
            else
            {
                var error = new ErrorModel
                {
                    Title = "Purchase Failed",
                    Message = "Buyer doesnt have enough Credit"
                };
                return BadRequest(error);
            }


        }


        public bool Save() {

            try
            {
                return (_db.SaveChanges() >= 0);
            }
            catch (DbUpdateException)
            {            
                    throw;
                
            }
        }

        private bool AppartmentExists(int id)
        {
            return _db.Appartments.Count(e => e.id == id) > 0;
        }


    }
}