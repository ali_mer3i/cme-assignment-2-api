﻿using CMEAssignment2Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace CMEAssignment2Api.Enteties
{
    public class Db_Context: DbContext
    {
        public Db_Context(DbContextOptions<Db_Context> options): base(options) {

        }
        public Db_Context() : base()
        {

        }
        public DbSet<Appartments> Appartments { get; set; }
        public DbSet<BuyersEntity> Buyers { get; set; }
        public DbSet<OwnedProperties> OwnedProperties { get; set; }


    }
}
