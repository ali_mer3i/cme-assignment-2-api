﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMEAssignment2Api.Models
{
    public class AppartmentsFilterModel
    {
        [JsonProperty("fromPrice")]
        public Decimal? FromPrice { get; set; }

        [JsonProperty("toPrice")]
        public Decimal? ToPrice { get; set; }

        [JsonProperty("address")]
        public String Address { get; set; }

        [JsonProperty("FromPrice")]
        public int? NbOfRooms { get; set; }

    }
}
