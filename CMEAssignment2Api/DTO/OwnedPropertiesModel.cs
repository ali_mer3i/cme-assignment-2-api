﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMEAssignment2Api.Models
{
    public class OwnedPropertiesModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("buyerId")]
        public int BuyerId { get; set; }

        [JsonProperty("apartmentId")]
        public int ApartmentId { get; set; }

    }
}
