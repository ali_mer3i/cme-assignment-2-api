﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMEAssignment2Api.Models
{
    public class AppartmentModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }


        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("nbOfRooms")]
        public int NbOfRooms { get; set; }

    }
}
