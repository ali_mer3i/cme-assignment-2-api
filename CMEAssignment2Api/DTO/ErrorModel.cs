﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMEAssignment2Api.Models
{
    public class ErrorModel
    {
        [JsonProperty("title")]
        public String Title { get; set; }

        [JsonProperty("message")]
        public String Message { get; set; }
    }
}
